from common import init
import time


class TestLoing(init.Init):

    def test_login(self):
        """失败用力"""
        self.add_img()
        self.click_by_link_text('登录')
        self.add_img()
        self.clear_by_id('user_login')
        self.input_by_id('user_login', 'fuwuyong')
        self.clear_by_id('user_password')
        self.input_by_id('user_password', '***')
        self.click_by_name('commit')
        time.sleep(1)
        self.assertEqual(self.driver.title, 'TesterHome1')

import unittest

from selenium import webdriver

from config import settings


class Init(unittest.TestCase):

    def setUp(self):
        self.imgs = []
        self.driver = webdriver.Chrome('static/driver/chromedriver.exe')
        self.driver.get(settings.base_url)
        self.driver.maximize_window()
        self.driver.implicitly_wait(30)

    def tearDown(self):
        self.driver.close()

    def clicl_by_id(self, id):
        self.driver.find_element_by_id(id).click()

    def click_by_name(self, name):
        self.driver.find_element_by_name(name).click()

    def click_by_class(self, class_name):
        self.driver.find_element_by_class_name(class_name).click()

    def click_by_xpath(self, xpath):
        self.driver.find_element_by_xpath(xpath).click()

    def click_by_link_text(self, link_text):
        self.driver.find_element_by_link_text(link_text).click()

    def clear_by_id(self, id):
        self.driver.find_element_by_id(id).clear()

    def clear_by_name(self, name):
        self.driver.find_element_by_name(name).clear()

    def clear_by_class(self, class_name):
        self.driver.find_element_by_class_name(class_name).clear()

    def clear_by_xpath(self, xpath):
        self.driver.find_element_by_xpath(xpath).clear()

    def clear_by_link_text(self, link_text):
        self.driver.find_element_by_link_text(link_text).clear()

    def input_by_id(self, id, in_value):
        self.driver.find_element_by_id(id).send_keys(in_value)

    def input_by_name(self, name, in_value):
        self.driver.find_element_by_name(name).send_keys(in_value)

    def input_by_class(self, class_name, in_value):
        self.driver.find_element_by_class_name(class_name).send_keys(in_value)

    def input_by_xpath(self, xpath, in_value):
        self.driver.find_element_by_xpath(xpath).send_keys(in_value)

    def input_by_link_text(self, link_text, in_value):
        self.driver.find_element_by_link_text(link_text).send_keys(in_value)

    def add_img(self):
        self.imgs.append(self.driver.get_screenshot_as_base64())
        return True

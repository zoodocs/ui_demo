import os
import sys
import unittest

from loguru import logger

from tools import HTMLTestRunner_chart


if __name__ == '__main__':

    BASE_DIR = os.path.dirname(os.path.abspath(__file__))
    sys.path.append(BASE_DIR)

    app_name = BASE_DIR.split(os.sep)[-1]

    logs_dir = os.path.join(BASE_DIR, 'logs')
    if os.path.exists(logs_dir) is False:
        os.makedirs(logs_dir)
    log_file = os.path.join(logs_dir, '{}.{}.log'.format(
        app_name, '{time:YYYY-MM-DD}'))
    # log_format = '{time:YYYY-MM-DD HH:mm:ss} | {level} | {message}'
    logger.add(log_file, rotation='1 days', retention='7 days',
               compression='zip', encoding='utf-8')

    testcases_path = os.path.join(BASE_DIR, 'testcases')
    discover = unittest.TestLoader().discover(
        testcases_path, 'test_input.py', top_level_dir=None)

    report_path = os.path.join(BASE_DIR, 'reports', 'ui_demo.html')

    with open(report_path, 'wb') as f:
        runner = HTMLTestRunner_chart.HTMLTestRunner(
            stream=f,
            verbosity=2,
            title='自动化测试报告',
            save_last_try=True,
            retry=0,
            description='描述嘻嘻',
            paroject_dir=BASE_DIR
        )
        runner.run(discover)
